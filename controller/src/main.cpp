#include <Arduino.h>

#define MOTOR_PIN 2
#define SPEED_PIN 4
#define UP_PIN    5
#define DOWN_PIN  6
#define AC_PIN    8
#define INC_PIN   9

static bool ac_enabled = false;

void setup()
{
  pinMode(MOTOR_PIN, OUTPUT);
  pinMode(UP_PIN, OUTPUT);
  pinMode(DOWN_PIN, OUTPUT);
  pinMode(AC_PIN, OUTPUT);

  pinMode(SPEED_PIN, INPUT);
  pinMode(INC_PIN, INPUT);

  digitalWrite(MOTOR_PIN, LOW);
  digitalWrite(UP_PIN, LOW);
  digitalWrite(DOWN_PIN, LOW);
  digitalWrite(AC_PIN, LOW);

  Serial.begin(9600);
}

void enable_ac()
{
  digitalWrite(AC_PIN, HIGH);
  ac_enabled = true;
}

void loop()
{
  if(Serial.available() > 0)
  {
    char c = Serial.read();
    switch(c)
    {
      case 'S':
      {
        while(!Serial.available());
        int speed = Serial.read();

        if((speed > 0) && !ac_enabled)
        {
          digitalWrite(AC_PIN, HIGH);
          ac_enabled = true;
        }

        analogWrite(MOTOR_PIN, speed);

        if((speed == 0) && ac_enabled)
        {
          digitalWrite(AC_PIN, LOW);
          ac_enabled = false;
        }
        Serial.println("Speed set");
      }
      break;

      case 'T':
      {
        digitalWrite(AC_PIN, HIGH);
        ac_enabled = true;

        analogWrite(MOTOR_PIN, 50);
      }
      break;

      case 'A':
      {
        digitalWrite(AC_PIN, LOW);
        ac_enabled = false;
      }
      break;

      case 'U':
      {
        digitalWrite(UP_PIN, HIGH);
        delay(1000);
        digitalWrite(UP_PIN, LOW);
        Serial.println("Move up");
      }
      break;

      case 'D':
      {
        digitalWrite(DOWN_PIN, HIGH);
        delay(1000);
        digitalWrite(DOWN_PIN, LOW);
        Serial.println("Move down");
      }
      break;
    }
  }
}
